#!/bin/sh
gcloud compute instances create batagor-1 \
    --zone=asia-east1-a --custom-cpu=6 --custom-memory=24 --custom-vm-type=e2

gcloud compute instances create batagor-2 \
    --zone=us-west1-a --custom-cpu=6 --custom-memory=24 --custom-vm-type=e2

gcloud compute instances create bagagor-3 \
    --zone=asia-east1-b --custom-cpu=6 --custom-memory=24 --custom-vm-type=e2

gcloud compute instances create batagor-4 \
    --zone=europe-north1-a --custom-cpu=6 --custom-memory=24 --custom-vm-type=e2

gcloud compute instances create batagor-5 \
    --zone=europe-west1-b --custom-cpu=6 --custom-memory=24 --custom-vm-type=e2